package mv;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import modelo.Traza;

import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;

import util.Conexion;

import com.j256.ormlite.dao.DaoManager;

public class PerezosoControlador extends SelectorComposer<Component> {
	
	private static final long serialVersionUID = 1L;
	
	@Wire
    private Textbox cedulaBox;
	
	@Wire
    private Intbox diasBox;
	
	@Wire
    private Intbox elementosBox;

	private int pesoMinimo;	
	
	public PerezosoControlador() {
		super();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream input = classLoader.getResourceAsStream("configuracion.properties");
			Properties properties = new Properties();
			properties.load(input);			
			pesoMinimo = Integer.parseInt(properties.getProperty("peso_minimo"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Listen("onUpload = #archivoButton")
	public void procesarArchivo(UploadEvent event){	
			try{				
			    String cedula = cedulaBox.getValue();
			    if(cedula==""){
			    	Messagebox.show("Debe ingresar una cedula válida", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
			    	return;
			    }
				String[] lineas = event.getMedia().getStringData().split(System.getProperty("line.separator"));
				StringBuffer stream = new StringBuffer();
				int elementos = -1;
				int totalElementos = 0;
			    int caso = 0;
			    List<Integer> elementosDia = new ArrayList<Integer>();
			    if( lineas.length > 3){
			    	diasBox.setValue(Integer.parseInt(lineas[0]));
			    	if(diasBox.getValue()< 1 || diasBox.getValue() > 500 ){
				    	Messagebox.show("Los dias deben estar entre 1 y 500", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
				    	return;
				    }
			    	for(int i = 1; i < lineas.length; i++){
			    		elementos = Integer.parseInt(lineas[i]);
			    		totalElementos += elementos;
			    		i++;
			    		int p = 0;
				    	for(int j=0; j < elementos; j++){
				    		elementosDia.add(Integer.parseInt(lineas[i+j]));
				    		p = j;
				    	}
				    	caso++;
				    	if( elementosDia.size() < 1 || elementosDia.size() > 100 ){
					    	Messagebox.show("Los elementos deben estar entre 1 y 100", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
					    	return;
					    }
				    	stream.append("Case #"+caso+":"+calcularViajes(elementosDia)+"\n");
				    	elementosDia.clear();		    	
				    	i = i + p;
				    }	    	
			    }else{
			    	Messagebox.show("El archivo debe tener almenos 3 lineas", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
			    }
			    elementosBox.setValue(totalElementos);
			    byte[] bytes = stream.toString().getBytes();
			    InputStream s = new ByteArrayInputStream(bytes);
			    Filedownload.save(s, "text/txt","output.txt");			    			
			    Traza traza = new Traza(cedula,stream.toString());
			    DaoManager.createDao(Conexion.getInstancia(), Traza.class).create(traza);				
			}catch(Exception e){
				
			}
		
	}
	
	public int calcularViajes(List<Integer> elementosDia){
		int viajes = 0;
		Collections.sort(elementosDia,Collections.reverseOrder());
		int auxiliar = 0;
		for(int i = 0; i < ( elementosDia.size() - auxiliar); i++){			
			int peso = elementosDia.get(i);
			if( elementosDia.size() < 1 || elementosDia.size() > 100 ){
		    	Messagebox.show("Los elementos deben pesar estar entre 1 y 100", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
		    	return 0;
		    }
			viajes++;
			if(peso < pesoMinimo){
				auxiliar =  auxiliar + ((pesoMinimo / peso) - 1);
				if((pesoMinimo % peso) > 0){
					auxiliar++;
				}
			}
			int restantes = ((elementosDia.size() - auxiliar) - i) - 1;
			int siguiente = elementosDia.get(i+1);
			if((siguiente*restantes) < pesoMinimo){
				break;
			}
		}
		return viajes;		
	}
}

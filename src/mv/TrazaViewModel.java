package mv;
 
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import util.Conexion;

import modelo.Traza;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;

 
public class TrazaViewModel {
    
	private Date fecha;
    private String cedula;
    private List<Traza> perezosoLista = new ArrayList<Traza>();
    public Traza seleccionado;
    
    
     
    public TrazaViewModel() {
    	try {
			Dao<Traza, String> perezosoDao = DaoManager.createDao(Conexion.getInstancia(), Traza.class);
			for (Traza traza : perezosoDao) {
			    perezosoLista.add(traza);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getCedula() {
		return cedula;
	}
	
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public List<Traza> getPerezosoLista() {
		return perezosoLista;
	}

	public void setPerezosoLista(List<Traza> perezosoLista) {
		this.perezosoLista = perezosoLista;
	}
	
	public Traza getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Traza seleccionado) {
		this.seleccionado = seleccionado;
	}
}
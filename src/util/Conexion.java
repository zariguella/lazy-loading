package util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

import com.j256.ormlite.jdbc.JdbcConnectionSource;

public class Conexion {
	
	static JdbcConnectionSource connectionSource = null;
	
	public static JdbcConnectionSource getInstancia(){
		if (connectionSource == null){
			try {
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				InputStream input = classLoader.getResourceAsStream("configuracion.properties");
				Properties properties = new Properties();				
				properties.load(input);					
				connectionSource = new JdbcConnectionSource(properties.getProperty("servidor"));
				input.close();
			} catch (SQLException | IOException e) {
				e.printStackTrace();
			}			
		}
		return connectionSource;
	}

}

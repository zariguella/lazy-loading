package modelo;

import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "traza")
public class Traza {
    @DatabaseField(id = true)
    public String cedula;
    
    @DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    public Date fecha;
    
    @DatabaseField(canBeNull = false)
    public String archivo;
    
    public Traza() {

    }   
    
	public String getCedula() {
		return cedula;
	}
	
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public Traza(String cedula, String archivo) {
		super();
		this.cedula = cedula;
		this.archivo = archivo;
		this.fecha = new Date();
	}   
    
}
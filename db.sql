CREATE TABLE public.traza
(
  cedula character varying(255) NOT NULL,
  fecha character varying(50),
  archivo character varying(255) NOT NULL,
  CONSTRAINT traza_pkey PRIMARY KEY (cedula)
)